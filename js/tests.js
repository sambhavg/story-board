/**
 * QUnit tests for the data objects
 */

test("test story data", function() {
    expect(4);
    var oldlen = db.stories.length;
    var newstoryid = db.next_story_id;
    var sprint_id = db.next_sprint_id-1;
    var short_desc = "test";
    var long_desc = "test long";
    var status = 0;
    var user = -1;

    // Test add
    db.addNewStory(sprint_id,short_desc,long_desc,status,user);
    deepEqual(db.stories.length,oldlen+1,"Adding new story");

    // Test retrieve
    var story = db.getStory(newstoryid);
    deepEqual(story.short_desc,short_desc,"Retrieve added story");

    // Test edit
    status = 1;
    db.editStory(newstoryid, short_desc, long_desc, status, user, sprint_id);
    story = db.getStory(newstoryid);
    deepEqual(story.status,status,"Update status");

    // Test delete
    db.deleteStory(newstoryid);
    deepEqual(db.stories.length,oldlen,"Delete new story");

    // Cleanup
    db.next_story_id--;
    store.set('next_story_id', db.next_story_id);
});

test("test sprint data", function() {
    expect(4);
    var oldlen = db.sprints.length;
    var newsprintid = db.next_sprint_id;
    var short_desc = "test";
    var long_desc = "test long";

    // Test add
    db.addNewSprint(short_desc,long_desc);
    deepEqual(db.sprints.length,oldlen+1,"Adding new sprint");

    // Test retrieve
    var sprint = db.getSprint(newsprintid);
    deepEqual(sprint.short_desc,short_desc,"Retrieve added sprint");

    // Test edit
    long_desc = "test long2";
    db.editSprint(newsprintid, short_desc, long_desc);
    sprint = db.getSprint(newsprintid);
    deepEqual(sprint.long_desc,long_desc,"Update long_desc");

    // Test delete
    db.deleteSprint(newsprintid);
    deepEqual(db.sprints.length,oldlen,"Delete new sprint");

    // Cleanup
    db.next_sprint_id--;
    store.set('next_sprint_id', db.next_sprint_id);
});

test("test user data", function() {
    expect(3);
    var oldlen = db.users.length;
    var newuserid = db.next_user_id;
    var name = "Rob";
    var role = "Dev";

    // Test add
    db.addNewUser(name,role);
    deepEqual(db.users.length,oldlen+1,"Adding new user");

    // Test retrieve
    var user = db.getUser(newuserid);
    deepEqual(user.name,name,"Retrieve added user");

    // Test edit
    role = "QA";
    db.editUser(newuserid, name, role);
    user = db.getUser(newuserid);
    deepEqual(user.role,role,"Update role");

    // Delete not allowed by design, no test

    // Cleanup
    db.next_user_id--;
    store.set('next_user_id', db.next_user_id);

    newusers = new Array();
    for (var i=0; i<oldlen; i++) {
        newusers.push(db.users[i]);
    }
    db.users = newusers;
    db.storeUsers();
});

test("test conversion of status column id to status int", function() {
    expect(5);
    var items = new Array();
    items.push("not_started_col");
    items.push("in_progress_col");
    items.push("in_testing_col");
    items.push("completed_col");

    for (var i=0; i<items.length; i++) {
        var res = db.getStatusId(items[i]);
        deepEqual(res,i,items[i] + " check");
    }

    var res = db.getStatusId("blahblahbalh");
    deepEqual(res,-1,"default check");
});

test("test conversion of status option string to status int", function() {
    expect(5);
    var items = new Array();
    items.push("Not Started");
    items.push("In Progress");
    items.push("In Testing");
    items.push("Completed");

    for (var i=0; i<items.length; i++) {
        var res = db.getStatusIdForOptions(items[i]);
        deepEqual(res,i,items[i] + " check");
    }

    var res = db.getStatusId("blahblahbalh");
    deepEqual(res,-1,"default check");
});