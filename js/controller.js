var controller = new Object();

// Global var to determine which Sprint we are working in
controller.current_sprint = null;

// Initial view setup
controller.initializeView = function () {
    //store.clear();
    $('#header').hide();
    $('#board').hide();
    $('#edit_current_sprint').hide();
    $('#add_story_action').hide();  // only hides the add_story when we still haven't selected a sprint, but allows you to add a sprint

    $("#sprint-list").empty().append(this.buildSprintListHTML());

    // create drop-down options for adding stories:
    $("#id_sprint_add_story").empty().append(controller.buildSprintOptionsHTML(-1));
    $("#id_user_add_story").empty().append(controller.buildUserOptionsHTML(-1));

};

// Shows/hides interface elements based on the user selecting a specific sprint
controller.selectSprint = function(sprint_id) {
    this.current_sprint = db.getSprint(sprint_id);
    this.refreshStoryBoard(sprint_id);
    var some_html = '<p> Sprint number ' + this.current_sprint.id
                + ' Starts on ' + this.current_sprint.start_date + ' and ends on '
                + this.current_sprint.end_date + '</p>' ;

    $('#header').show();
    $('#board').show();
    $('#add_story_action').show();
    $('#edit_current_sprint').show();
};

// Updates the storyboard when an event has occurred that might need to be reflected on the board
controller.refreshStoryBoard = function(sprint_id) {
    var builtHTML = this.buildStoryBoardHTML(sprint_id);
    $("#whichsprint").empty().append('Current Sprint: ' + sprint_id);
    $("#not_started_col").empty().append(builtHTML.not_started);
    $("#in_progress_col").empty().append(builtHTML.in_progress);
    $("#in_testing_col").empty().append(builtHTML.in_testing);
    $("#completed_col").empty().append(builtHTML.completed);
    $(".slist").sortable({
        items: "div.slist-item",
        connectWith: ".slist",
        placeholder: "ui-state-highlight",
        revert: true,
        update: function (event, ui) {
            if (ui.item.length > 0) {
                var und_ind = ui.item.attr('id').indexOf('_');
                var story_id = ui.item.attr('id').substring(und_ind+1);
                var parent = ui.item.parent();
                var new_status = parent.attr('id');
                db.updateStory(story_id, new_status);
            }
        }
    }).disableSelection();
};

// Utility function to build the sprint selection buttons
controller.buildSprintListHTML = function (){
        var sprinthtml = "";
        for (var i=0; i<db.sprints.length; i++) {
            sprinthtml += '<div><button type="button" class="btn btn-default" ' +
                'onclick="controller.selectSprint('+db.sprints[i].id+')" data-dismiss="modal">Sprint '+i+': '+db.sprints[i].short_desc+'</button>';
            sprinthtml += '&nbsp<a href="#" onclick="controller.editSprint('+db.sprints[i].id+')" class="glyphicon glyphicon-cog"></a></div>';
        }
    return sprinthtml;
};

// Utility function that generates the board's html elements
controller.buildStoryBoardHTML = function (sprint_id){
    var not_started = "";
    var in_progress = "";
    var in_testing = "";
    var completed = "";
    for(var i = 0 ; i < db.stories.length; i++){
        if (sprint_id == db.stories[i].sprint) {
            switch (db.stories[i].status){
                case 0:
                    not_started += this.buildStoryHTML(db.stories[i]);
                    break;
                case 1:
                    in_progress += this.buildStoryHTML(db.stories[i]);
                    break;
                case 2:
                    in_testing += this.buildStoryHTML(db.stories[i]);
                    break;
                case 3:
                    completed += this.buildStoryHTML(db.stories[i]);
                    break;
            }
        }
    }
    var builtHTML = new Object();
    builtHTML.not_started = not_started;
    builtHTML.in_progress = in_progress;
    builtHTML.in_testing = in_testing;
    builtHTML.completed = completed;
    return builtHTML;
};

// Generates each story's html element
controller.buildStoryHTML = function (story){
    var user = db.getUser(story.user);
    var storyHTML = '<div class="slist-item boarditems" id="story_' + story.id + '">';
    storyHTML += '<div class="panel panel-default">' +
                    '<div class="panel-heading">' +
                        '<p class="h3 panel-title">' + story.short_desc +
                        '<a href="#" onclick="controller.editStory('+story.id+')" class="glyphicon glyphicon-cog pull-right"></a></p>' +
                    '</div>';
    if(user != null){
        storyHTML +=    '<div class="panel-body">' + user.name + '</div>';
    } else {
        storyHTML +=    '<div class="panel-body">' + 'NO USER ASSIGNED!</div>';
    }
    storyHTML += '</div>';
    storyHTML += '</div>';
    return storyHTML;
};

// Called when user presses "Add Story" button.
// Opens up the "Add Story" input modal.
controller.addStory = function (){
    if(this.current_sprint != null){
        $("#addStory").modal('show');
    }
};

// Called when user presses "Save Story" button.
// Reads in the data entered by the user in the "Add Story" input modal.
// Calls the creation of a new story and refreshes story board.
controller.saveStory = function (){
    if(this.current_sprint != null){
        var short_desc = $("#id_short_desc").val();
        var long_desc = $("#id_long_desc").val();
        var status = db.getStatusIdForOptions($("#id_status").val());     
        var user = parseInt($("#id_user_add_story").val());
        if($("#id_sprint_add_story").val() == ""){
            sprint = this.current_sprint.id;
        } else {
            sprint = parseInt($("#id_sprint_add_story").val());
        }
        db.addNewStory(sprint, short_desc, long_desc, status, user);
        this.refreshStoryBoard(this.current_sprint.id);
    }
};

// Called when the user presses the edit icon on a story.
// Populates a modal with the story's details and shows the modal.
controller.editStory = function(story_id){
    var story = db.getStory(story_id);
    if(story != null){
        var status = story.status;
        var user = story.user;
        var sprint = story.sprint;        
        $("#edit_story_short_desc").val(story.short_desc);
        $("#edit_story_long_desc").empty().append(story.long_desc);
        $("#id_sprint_edit_story").empty().append(controller.buildSprintOptionsHTML(sprint));
        $("#edit_story_status").empty().append(controller.buildStatusOptionsHTML(status));
        $("#id_user_edit_story").empty().append(controller.buildUserOptionsHTML(user));
        $("#spanEditStory").empty().append(story_id);
        $("#deleteStoryButton").empty().append('<button type="button" class="btn btn-danger" onclick="controller.deleteStory(' +
            story_id + ')" data-dismiss="modal">Delete This Story</button>');
        $("#editStory").modal('show');
    }
};

// Called when the user presses the "Save Changes to the Story" button.
// Reads the new story's details, calls an edit on the story, and refreshes the story board.
controller.saveChangesStory = function(){
    var story_id = $("#spanEditStory").text();
    if(story_id >=0 && story_id < db.next_story_id){
        var short_desc = $("#edit_story_short_desc").val();
        if(short_desc == ""){
            short_desc = $("#span_edit_story").text();
        }
        var long_desc = $("#edit_story_long_desc").val();
        var status = db.getStatusIdForOptions($("#edit_story_status").val());
        var user = $("#id_user_edit_story").val();
        var sprint = $("#id_sprint_edit_story").val();
        sprint = +sprint; //unary + converts strings into ints
        db.editStory(story_id, short_desc, long_desc, status, user, sprint);
        controller.refreshStoryBoard(controller.current_sprint.id);
    }
};

// Called when the user hits the 'delete' button in the Edit Story modal
controller.deleteStory = function(story_id) {
    db.deleteStory(story_id);
    controller.refreshStoryBoard(controller.current_sprint.id);
};

// Called when user presses "Add Sprint" button.
// Opens up the "Add Sprint" input modal.
controller.addSprint = function (){
    $("#addSprint").modal('show');
};

// Called when the user presses the "Save Sprint" button.
// Collects the sprint information and calls the creation of a new sprint.
controller.saveSprint = function(){
    var short_desc = $("#sprint_short_desc").val();
    var long_desc = $("#sprint_long_desc").val();
    db.addNewSprint(short_desc, long_desc);
    // update the list of selectable sprints in the "Select Sprint" modal
    $("#sprint-list").empty().append(this.buildSprintListHTML());
    // update the list of sprint options for adding story
    $("#id_sprint_add_story").empty().append(controller.buildSprintOptionsHTML(-1));
};

// Called when the user hits the gear icon in the Select Sprint modal, presents the Edit Sprint modal
controller.editSprint = function(sprint_id){
    var sprint = db.getSprint(sprint_id);

    $("#edit_sprint_short_desc").val(sprint.short_desc);
    $("#edit_sprint_long_desc").empty().append(sprint.long_desc);

    $("#deleteSprintButton").empty().append('<button type="button" class="btn btn-danger" onclick="controller.deleteSprint(' +
        sprint_id + ')" data-dismiss="modal">Delete This Sprint</button>');

    $("#edit_sprint_footer").empty().append(this.buildEditSprintFooterHTML(sprint_id));

    $("#editSprint").modal('show');
};

// Called when the user hits the Edit This Sprint menu item
controller.editCurrentSprint = function() {
    if (this.current_sprint != null) {
        this.editSprint(this.current_sprint.id);
    }
};

// Updates the Sprint after the user has made changes with the Edit Sprint modal
controller.saveChangesSprint = function(sprint_id){
    var short_desc = $("#edit_sprint_short_desc").val();
    var long_desc = $("#edit_sprint_long_desc").val();
    db.editSprint(sprint_id, short_desc, long_desc);
    // update the list of selectable sprints in select sprint modal
    $("#sprint-list").empty().append(this.buildSprintListHTML());
    // update the list of sprint options for adding story
    $("#id_sprint_add_story").empty().append(controller.buildSprintOptionsHTML(-1));
};

// Deletes the sprint, its stories, and refreshes the view as necessary
controller.deleteSprint = function(sprint_id) {
    db.deleteSprint(sprint_id);

    // If we deleted the currently selected sprint, reset the view to have no selected sprint
    if (this.current_sprint != null) {
        if (sprint_id == this.current_sprint.id) {
            this.initializeView();
            $("#whichsprint").empty().append('No Sprint Selected');
            this.current_sprint = null;
        }
    }

};

// Called when user presses "Add User" button.
// Opens up the "Add User" input modal.
controller.addUser = function(){
    $("#addUser").modal('show');
};

// Called when the user presses the "Save User" button.
// Collects the User information and calls the creation of a User.
controller.saveUser = function(){
    var u_name = $("#user_name").val();
    var role = $("#user_role").val();;
    db.addNewUser(u_name, role);
    // update the list of User options for adding stories
    $("#id_user_add_story").empty().append(controller.buildUserOptionsHTML(-1));
};

// Called when the user presses the "Edit User" button.
// Builds and displays a modal which lists all available Users.
controller.selectUser = function(){
    $("#id_user_select_user").empty().append(controller.buildUserOptionsHTML(-1));
    $("#selectUser").modal('show');
};

// Called when the user presses the "Select User" button.
// Populates a modal with the User's details and shows the modal.
controller.editUser = function(){
    var user_id = parseInt($("#id_user_select_user").val());
    var user = db.getUser(user_id);
    if(user != null){
        $("#spanEditUser").empty().append(user_id);
        $("#user_name_edit").val(user.name);
        $("#user_role_edit").empty().append(user.role);
        $("#editUser").modal('show');
    }
};

// Called when the user presses the "Save Changes to the User" button.
// Reads in the new User information and calls an edit to the User and refreshes the story board.
controller.saveChangesUser = function(){
    var user_id = parseInt($("#spanEditUser").text());
    var u_name = $("#user_name_edit").val();
    if(u_name == ""){
        u_name = $("#span_edit_user").text();
    }
    var role = $("#user_role_edit").text();
    db.editUser(user_id, u_name, role);
    // update the list of options for add stories
    $("#id_user_add_story").empty().append(controller.buildUserOptionsHTML(-1));
    this.refreshStoryBoard(this.current_sprint.id);
};

// Function to populate the User options in the drop down menus.
// the user_id is used to determine if a User option should be selected or not
controller.buildUserOptionsHTML = function (user_id){
    var user_html = "";
    // if the user_id is not in the list of Users display ------- as selected option
    if(user_id < 0 || user_id >= db.next_user_id){
        user_html += '<option value="" selected="selected">---------</option>';
        for (var i=0; i<db.users.length; i++) {
            user_html += '<option value="'+i+'">'+db.users[i].name+'</option>';
        }
    } else {
        user_html += '<option value="" >---------</option>';
        for (var i=0; i<db.users.length; i++) {
            if(i == user_id){
                user_html += '<option value="'+i+'" selected="selected" >'+db.users[i].name+'</option>';
            } else {
                user_html += '<option value="'+i+'">'+db.users[i].name+'</option>';
            }
        }
    }
    return user_html;
};

// Function to populate the sprint options in the drop down menus.
// The sprint_id is used to determine if a sprint option should be selected or not
controller.buildSprintOptionsHTML = function (sprint_id){
    var sprint_html = "";
    // if the sprint_id is not in the list of sprints display ------- as selected option
    if(sprint_id < 0 || sprint_id >= db.next_sprint_id){
        sprint_html += '<option value="" selected="selected">---------</option>';
        for (var i=0; i<db.sprints.length; i++) {
            sprint_html += '<option value="'+i+'">Sprint '+i+': '+db.sprints[i].short_desc+'</option>';
        }
    } else {
        sprint_html += '<option value="" >---------</option>';
        for (var i=0; i<db.sprints.length; i++) {
            if(i == sprint_id){
                sprint_html += '<option value="'+i+'" selected="selected" >Sprint '+i+': '+db.sprints[i].short_desc+'</option>';
            } else {
                sprint_html += '<option value="'+i+'">Sprint '+i+': '+db.sprints[i].short_desc+'</option>';
            }
        }
    }
    return sprint_html;
};

// Function to populate the status options in the edit drop down menus.
// The status_id is used to determine if a status option should be selected or not.
controller.buildStatusOptionsHTML = function (status_id){
    var status_html = "";
    // if the status_id is not in the list of statuses display ------- as selected option
    if(status_id >= 0 && status_id <= 3){
        status_html += '<option value="" >---------</option>';
        switch (status_id){
            case 0:
                status_html += '<option value="Not Started" selected="selected">Not Started</option>' +
                		'<option value="In Progress">In Progress</option>' +
                		'<option value="In Testing">In Testing</option>' +
                		'<option value="Completed">Completed</option>';
                break;
            case 1:
                status_html += '<option value="Not Started">Not Started</option>' +
                		'<option value="In Progress" selected="selected">In Progress</option>' +
                		'<option value="In Testing">In Testing</option>' +
                		'<option value="Completed">Completed</option>';
                break;
            case 2:
                status_html += '<option value="Not Started">Not Started</option>' +
                		'<option value="In Progress">In Progress</option>' +
                		'<option value="In Testing" selected="selected">In Testing</option>' +
                		'<option value="Completed">Completed</option>';
                break;
            case 3:
                status_html += '<option value="Not Started">Not Started</option>' +
                		'<option value="In Progress">In Progress</option>' +
                		'<option value="In Testing">In Testing</option>' +
                		'<option value="Completed" selected="selected">Completed</option>';
                break;
        }

    } else {
        status_html += '<option value="" selected="selected">---------</option>';
        status_html += '<option value="Not Started">Not Started</option>' +
                		'<option value="In Progress">In Progress</option>' +
                		'<option value="In Testing">In Testing</option>' +
                		'<option value="Completed">Completed</option>';
    }
    return status_html;
};

// Builds the edit sprint modal action buttons
controller.buildEditSprintFooterHTML = function(sprint_id) {
    var html = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '<button type="submit" class="btn btn-primary" data-dismiss="modal" onclick="controller.editSprint(' + sprint_id + ')">Save Sprint</button>';
    return html;
};
