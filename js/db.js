window.db = new Object();

// Called anytime a User is added or changes are saved
db.storeUsers = function(){
    store.set('users',db.users);
};

// Called anytime a sprint is added or changes are saved
db.storeSprints = function(){
    store.set('sprints',db.sprints);
};

// Called anytime a story is added or changes are saved
db.storeStories = function() {
    store.set('stories',db.stories);
};

// Loads the next_sprint_id variable from storage or if it does not exist, assigns it a value of zero.
db.next_sprint_id = store.get('next_sprint_id');
if (!db.next_sprint_id) {
    db.next_sprint_id = 0;
}

// Loads the next_story_id variable from storage or if it does not exist, assigns it a value of zero.
db.next_story_id = store.get('next_story_id');
if (!db.next_story_id) {
    db.next_story_id = 0;
}

// Loads the next_user_id variable from storage or if it does not exist, assigns it a value of zero.
db.next_user_id = store.get('next_user_id');
if (!db.next_user_id) {
    db.next_user_id = 0;
}

// Returns the next available ID and advances the variable to the next unused ID.
db.nextSprintID = function(){
    var r = db.next_sprint_id;
    db.next_sprint_id++;
    store.set('next_sprint_id',db.next_sprint_id);
    return r;
};

// Returns the next available ID and advances the variable to the next unused ID.
db.nextStoryID = function(){
    var r = db.next_story_id;
    db.next_story_id++;
    store.set('next_story_id',db.next_story_id);
    return r;
};

// Returns the next available ID and advances the variable to the next unused ID.
db.nextUserID = function(){
    var r = db.next_user_id;
    db.next_user_id++;
    store.set('next_user_id',db.next_user_id);
    return r;
};

// If no data already exists in the users cache, create some data to play with
db.populateSprints = function() {
    for(var i = 0; i < 3; i++){
        var sprint = new Object();
        sprint.id = db.nextSprintID();
        sprint.short_desc = "Sprint " + i;
        sprint.long_desc = "Not very long.";
        sprint.start_date = "1";
        sprint.end_date = "10";
        db.sprints.push(sprint);
    }
    this.storeSprints();
};

// If no data already exists in the users cache, create some data to play with
db.populateUsers = function() {
    for(var i = 0; i < 3; i++){
        var user = new Object();
        user.name = "Plamen The " + (i+1) + "th";
        user.id = db.nextUserID();
        user.role = "developer";
        db.users.push(user);
    }
    this.storeUsers();
};

// If no data already exists in the users cache, create some data to play with
db.populateStories = function() {
    for(var i = 0; i < 10; i++){
        var story = new Object();
        story.id = db.nextStoryID();
        story.short_desc = "my name is " + i;
        story.long_desc = "I have no name";
        story.status = i%4;
        story.user = i%3;
        story.sprint = i%2;
        db.stories.push(story);
    }
    this.storeStories();
};

// If no data already exists in the users cache, create some data to play with
db.sprints = store.get('sprints');
if (!db.sprints) {
    db.sprints = new Array();
    db.populateSprints();
}

// If no data already exists in the users cache, create some data to play with
db.users = store.get('users');
if (!db.users) {
    db.users = new Array();
    db.populateUsers();
}

// If no data already exists in the users cache, create some data to play with
db.stories = store.get('stories');
if (!db.stories) {
    db.stories = new Array();
    db.populateStories();
}

// Called when a user wants to save changes to a story.
db.editStory = function(story_id, short_desc, long_desc, status, user, sprint) {
    var story = db.getStory(story_id);
    if(story != null){
        story.short_desc = short_desc;
        story.long_desc = long_desc;
        story.status = status;
        story.user = user;
        story.sprint = sprint;
    }
    this.storeStories();
};

// Called when a user wants to save changes to a sprint.
db.editSprint = function(sprint_id, short_desc, long_desc) {
    var sprint = db.getSprint(sprint_id);
    if (sprint != null) {
        sprint.short_desc = short_desc;
        sprint.long_desc = long_desc;
    }
    this.storeSprints();
};

// Called when a user wants to save changes to a user.
db.editUser = function(user_id, u_name, role){
    var user = db.getUser(user_id);
    if(user != null){
        user.name = u_name;
        user.role = role;
    }
    this.storeUsers();
};

// Given a User ID return the User object if it exists and null otherwise.
db.getUser = function(user_id) {
    for (var i=0; i<this.users.length; i++) {
        if (user_id == this.users[i].id)
            return this.users[i];
    }
    return null;
};

// Given a sprint ID return the sprint object if it exists and null otherwise.
db.getSprint = function(sprint_id) {
    for (var i=0; i<this.sprints.length; i++) {
        if (sprint_id == this.sprints[i].id)
            return this.sprints[i];
    }
    return null;
};

// Given a story ID return the story object if it exists and null otherwise.
db.getStory = function(story_id){
    for (var i=0; i<this.stories.length; i++) {
        if (story_id == this.stories[i].id)
            return this.stories[i];
    }
    return null;
};

// Called when a user wishes to delete a specific story
db.deleteStory = function(story_id) {
    var found_ind = -1;
    for (var i=0; i<this.stories.length; i++) {
        if (story_id == this.stories[i].id) {
            found_ind = i;
            break;
        }
    }
    if (found_ind != -1) {
        this.stories.splice(found_ind, 1);
    }
    this.storeStories();
};

// Called when the user has done a drag/drop event to change a story's status
db.updateStory = function(story_id, new_status) {
    for (var i=0; i<db.stories.length; i++) {
        if (story_id == db.stories[i].id) {
            var newid = this.getStatusId(new_status);
            if (newid != -1) {
                db.stories[i].status = newid;
                this.storeStories();
                return;
            }
        }
    }
};

// Utility function that converts the column on the board into a status integer
db.getStatusId = function(text_status) {
    switch (text_status) {
        case "not_started_col":
            return 0;
        case "in_progress_col":
            return 1;
        case "in_testing_col":
            return 2;
        case "completed_col":
            return 3;
        default:
            return -1;
    }
    return -1;
};

// Utility function that converts the option text on a dropdown into a status integer
db.getStatusIdForOptions = function(text_status){
switch (text_status) {
        case "Not Started":
            return 0;
        case "In Progress":
            return 1;
        case "In Testing":
            return 2;
        case "Completed":
            return 3;
        default:
            return -1;
    }
    return -1;
};

// Adds a new story to the list of stories
db.addNewStory = function(sprint_id, short_desc, long_desc, status, user){
    var story = new Object();
    story.id = db.nextStoryID();
    story.short_desc = short_desc;
    story.long_desc = long_desc;
    // if the input status is wrong, assign status = 0 ["Not Started"] to the current story
    if(isNaN(status) || status < 0 || status > 3 ){
        story.status = 0;
    } else {
        story.status = status;
    }
    // check if the user input is correct, if it is not, assign no user to the story [-1]
    if(isNaN(user) || user < 0 || user >= db.next_user_id){
        story.user = -1;
    } else {
        story.user = user;
    }
    story.sprint = sprint_id;
    db.stories.push(story);
    this.storeStories();
    store.set('next_story_id', this.next_story_id);
};

// Adds a new sprint to the list of sprints
db.addNewSprint = function(short_desc, long_desc){
    var sprint = new Object();
    sprint.id = db.nextSprintID();
    sprint.short_desc = short_desc;
    sprint.long_desc = long_desc;
    sprint.start_date = 1;
    sprint.end_date = 10;
    db.sprints.push(sprint);
    this.storeSprints();
    store.set('next_sprint_id',this.next_sprint_id);
};

// Adds a new User to the list of Users
db.addNewUser = function(u_name, role){
    var user = new Object();
    user.id = db.nextUserID();
    user.name = u_name;
    user.role = role;
    db.users.push(user);
    this.storeUsers();
    store.set('next_user_id',this.next_user_id);
};

//This deletes all stories associated with a sprint and the sprint itself.
db.deleteSprint = function(sprint_id) {
    var newstories = new Array();
    var found_ind = -1;

    // Delete stories
    for (var i=0; i<this.stories.length; i++) {
        if (sprint_id != this.stories[i].sprint) {
            newstories.push(this.stories[i]);
        }
    }
    this.stories = newstories;
    this.storeStories();

    // Delete sprint
    for (var i=0; i<this.sprints.length; i++) {
        if (this.sprints[i].id == sprint_id) {
            found_ind = i;
            break;
        }
    }
    if (found_ind != -1) {
        this.sprints.splice(found_ind, 1);
    }
    this.storeSprints();
};
